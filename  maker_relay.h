#ifndef _MAKER_RELAY_
#define _MAKER_RELAY_


/*
    Note: Reads the sensor value from the analog pin.

    @param
      pin is the digital pin on which the relay is conected.
	  state is value that is being sent to the relay
*/
void maker_relayState(uint8_t pin , uint8_t state);

#endif
