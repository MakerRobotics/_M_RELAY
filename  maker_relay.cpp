/*
 * Library: maker_relay.h
 *
 * Organization: MakerRobotics
 * Autors: Luka Caric
 *
 * Date: 3.12.2017.
 * Test: Arduino Nano
 *
 * Note: Tested on a 5V dual relay board
 * 
 */

#include "maker_relay.h"
#include "Arduino.h"
#include "stdint.h"


/* Changes the state of relay on pin*/
void maker_relayState(uint8_t pin, uint8_t state)
{
	digitalWrite(pin , state);
}
